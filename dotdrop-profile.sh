#!/bin/sh

# Usage:
#     profile=PROFILE_NAME dotdrop-profile.sh DOTDROP_ARGS
# This wrapper script calls dotdrop with a given profile PROFILE_NAME.
# Each profile can have a specific setup in script ‘./profiles/PROFILE_NAME.sh’.
# The intention is to allow setting profile-specifc environment variables for
# templating dot files.
# If omitted, the profile defaults to the hostname.

dotfiles_root="$(dirname "$BASH_SOURCE")"

: ${profile:=$(hostname)}
profile_file="$dotfiles_root/profiles/$profile.sh"

if [ -r "$profile_file" ] ; then
	set -a  # variables set from the included script will be exported to dotdrop
	source "$profile_file"
	set +a
else
	echo -e "\e[33mWarning: cannot read profile file: $profile_file\e[0m" >&2
fi

: ${DOTDROP_AUTOUPDATE:=no} ; export DOTDROP_AUTOUPDATE

./dotdrop.sh --profile="$profile" "$@"
DOTDROP_AUTOUPDATE=no exec sudo -E -u root ./dotdrop.sh --profile="${profile}_global" "$@"
