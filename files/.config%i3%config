# {{@@ DOTDROP_WARNING @@}}

# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!


# Show window icons.
# for_window [all] title_window_icon yes

# border width, in pixels:
set $border_width 3

# No title bar for windows, except if floating.
default_border          pixel $border_width
default_floating_border normal $border_width

# this does not appear to work:
#for_window [floating] border normal $border_width
#for_window [tiling]   border pixel $border_width

# this helps (suggested in http://stackoverflow.com/a/34433725):
set $set_float   floating enable, border normal $border_width
set $unset_float floating disable, border pixel $border_width

# Special treatments (use xprop(1) to retrieve window properties).
for_window [window_role="^alert$"] $set_float    # Thunderbird 128 notifications
for_window [class="^thunderbird$" window_role="^about$"] $set_float
for_window [class="^firefox$" window_role="^(?!browser$)"] $set_float
for_window [class="^Xsane$"] $set_float
for_window [class="^vlc$" window_role="^(?!vlc-main$|vlc-playlist$)"] $set_float
for_window [class="^MPlayer$"] $set_float
for_window [class="^feh$"] $set_float
for_window [class="^Gifview$"] $set_float

# i3 modifier key (Mod1 = Alt, Mod4 = Windows)
set $mod Mod4

# Colour scheme.
set $bg    #222222
set $bg_hl #333333
set $fg0    #888888
set $fg0_hl #ffffff
set $fg1    #00aa00
set $fg1_hl #80ff80
set $fg2    #4fc0c0
set $fg2_hl #55ffff

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below. ISO 10646 = Unicode
#font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, if you need a lot of unicode glyphs or
# right-to-left text rendering, you should instead use pango for rendering and
# chose a FreeType font, such as:
#font pango:DejaVu Sans Mono 9
font pango:Mono 8

# configure the display automatically
bindsym $mod+x exec --no-startup-id "if command -v autorandr >/dev/null 2>&1 ; then autorandr -c ; else xrandr --auto ; fi"

# set the screen brightness interactively
bindsym $mod+y exec --no-startup-id ~/bin/i3-set-xbacklight

# start dmenu (a program launcher)
bindsym $mod+space exec --no-startup-id dmenu_run_nofork -i -p » -sb "$bg_hl" -sf "$fg1_hl" &>~/.i3-dmenu.log
# start i3-demenu-desktop, a wrapper around dmenu which only displays those
# applications shipping a .desktop file
bindsym $mod+Shift+space exec --no-startup-id i3-dmenu-desktop --entry-type=name --dmenu="dmenu -i -p » -sb $bg_hl -sf $fg1_hl" &>~/.i3-dmenu-desktop.log

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# screenshot
bindsym Print exec --no-startup-id ~/bin/screenshot
bindsym $mod+Print exec --no-startup-id ~/bin/screenshot active

# kill focused window
bindsym $mod+q kill

# change focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# focus the parent or child container
bindsym $mod+{{@@ PRIOR_KEY @@}} focus parent
bindsym $mod+{{@@ NEXT_KEY @@}} focus child

# move focused window
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal or vertical orientation
bindsym $mod+h split h
bindsym $mod+v split v

# toggle fullscreen mode for the focused container
bindsym $mod+f fullscreen

# toggle border style
bindsym $mod+b border toggle $border_width

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+t layout tabbed
bindsym $mod+s layout stacking
bindsym $mod+n layout toggle split

# toggle tiling / floating
bindsym $mod+w floating toggle

# change focus between tiling / floating windows
bindsym $mod+z focus mode_toggle

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# resize window (you can also use the mouse for that)
bindsym $mod+d mode "resize"

mode "resize" {
	# These bindings trigger as soon as you enter the resize mode.

	# Pressing left will shrink the window’s width.
	# Pressing right will grow the window’s width.
	# Pressing up will shrink the window’s height.
	# Pressing down will grow the window’s height.
	bindsym Left resize shrink width 10 px or 10 ppt
	bindsym Down resize grow height 10 px or 10 ppt
	bindsym Up resize shrink height 10 px or 10 ppt
	bindsym Right resize grow width 10 px or 10 ppt

	# back to normal: Enter or Escape
	bindsym Return mode "default"
	bindsym Escape mode "default"
}

# rename the current workspace
bindsym $mod+c exec --no-startup-id ~/bin/i3-rename-workspace

# enable “back and forth” upon pressing the key for the current workspace?
#workspace_auto_back_and_forth yes

# switch to workspace (this maps to the key row carrying the ten digits)
bindcode $mod+49 workspace number 0
bindcode $mod+10 workspace number 1
bindcode $mod+11 workspace number 2
bindcode $mod+12 workspace number 3
bindcode $mod+13 workspace number 4
bindcode $mod+14 workspace number 5
bindcode $mod+15 workspace number 6
bindcode $mod+16 workspace number 7
bindcode $mod+17 workspace number 8
bindcode $mod+18 workspace number 9
bindcode $mod+19 workspace number 10
bindcode $mod+20 workspace prev
bindcode $mod+21 workspace next
bindsym $mod+Tab workspace back_and_forth

# move focused container to workspace
bindcode $mod+Shift+49 move container to workspace number 0
bindcode $mod+Shift+10 move container to workspace number 1
bindcode $mod+Shift+11 move container to workspace number 2
bindcode $mod+Shift+12 move container to workspace number 3
bindcode $mod+Shift+13 move container to workspace number 4
bindcode $mod+Shift+14 move container to workspace number 5
bindcode $mod+Shift+15 move container to workspace number 6
bindcode $mod+Shift+16 move container to workspace number 7
bindcode $mod+Shift+17 move container to workspace number 8
bindcode $mod+Shift+18 move container to workspace number 9
bindcode $mod+Shift+19 move container to workspace number 10
bindcode $mod+Shift+20 move container to workspace prev
bindcode $mod+Shift+21 move container to workspace next
bindsym $mod+Shift+Tab move container to workspace back_and_forth

# reload the configuration file
bindsym $mod+r reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+BackSpace exec --no-startup-id "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
# no confirmation
bindsym $mod+Shift+BackSpace exec --no-startup-id "i3-msg exit"

# Delay toggling the urgency hint off after switching to the hinted window:
force_display_urgency_hint 3000 ms

# Colours.
# class                 border  backgr. text    indicator
client.unfocused        $bg     $bg     $fg0    $bg
client.focused_inactive $bg     $bg     $fg0_hl $bg
client.focused          $bg_hl  $bg_hl  $fg1_hl #555555
client.urgent           $bg     $bg     $fg2_hl $bg

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available).
bar {
	position top
	#mode hide    # dock|hide|invisible

	#status_command i3status
	status_command measure-net-speed-i3status

	colors {
		background $bg
		statusline #eeeeee
		#separator #444444

		# class            border  backgr. text
		inactive_workspace $bg     $bg     $fg0
		active_workspace   $bg     $bg     $fg1
		focused_workspace  $bg     $bg     $fg1_hl
		urgent_workspace   $bg     $bg     $fg2_hl
	}
}

# Autostart programs (.desktop entries in /etc/xdg/autostart/).
#exec --no-startup-id dex -ae i3
# This is already done in ~/.xinitrc.

# Select the initial workspace.
exec --no-startup-id i3-msg workspace number 0
