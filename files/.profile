#
# ~/.profile
#

# This function prepends / appends "$1" to the PATH when not already in
# (adapted from the /etc/profile that ships with Archlinux).
add_path () {
	case ":$PATH:" in
		*":$2:"*)
			;;
		*)
			case "$1" in
				"pre")
					PATH="$2${PATH:+:$PATH}" ;;
				"post")
					PATH="${PATH:+$PATH:}$2"
			esac
			export PATH
	esac
}
prepend_path () { add_path pre "$@" ; }
append_path () { add_path post "$@" ; }

export -f add_path prepend_path append_path

# This function tidies up the PATH-like variable whose name is given
# (PATH, MANPATH, etc.). It removes duplicates, removes empty paths
# (which have most likely been added by mistake and are dangerous,
# since they actually mean the current directory) and it also takes
# special care of opam switches.
cleanup_paths() {
	# pathvar is a reference variable to the variable whose name is given:
	declare -n pathvar="$1"
	export pathvar="$(\
		cleaned_path=':'
		IFS=':'
		for elt in $pathvar ; do
			# remove empty paths and redundant paths (keep the earlier occurrence):
			[[ -z "$elt" || "$cleaned_path" == *":$elt:"* ]] && continue
			# opam: remove paths of the shape "$HOME/.opam/XXX/SUFFIX" if there
			# is an earlier path of the shape "$HOME/.opam/YYY/SUFFIX" (artifact
			# of switching between opam contexts):
			pref="$HOME/.opam"
			if [[ "$elt" == "$pref/"* ]] ; then
				suff="${elt#$pref/*/}"
				[[ "$cleaned_path" == *":$pref/"+([^/])"/$suff:"* ]] && continue
			fi
			cleaned_path+="$elt:"
		done
		echo -n "${cleaned_path:1:-1}"
	)"
}

export -f cleanup_paths

# add ~/bin to the search path:
test -d "$HOME/bin" && prepend_path "$HOME/bin"

# Perl:
if test -d "${HOME}/perl5" ; then
	PATH="${HOME}/perl5/bin${PATH+:}${PATH}"; export PATH;
	PERL5LIB="${HOME}/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
	PERL_LOCAL_LIB_ROOT="${HOME}/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
	PERL_MB_OPT="--install_base \"${HOME}/perl5\""; export PERL_MB_OPT;
	PERL_MM_OPT="INSTALL_BASE=${HOME}/perl5"; export PERL_MM_OPT;
fi

# OPAM:
test -r "$HOME/.opam/opam-init/init.sh" && . "$HOME/.opam/opam-init/init.sh" > /dev/null 2> /dev/null || true
# Note: As we are in a login, probably non‐interactive shell, the init script
# above only defines environment variables. For other features (auto‐completion
# and the function ‘opam-switch-eval’, it has to be sourced again when needed,
# for example from ~/.bashrc).

# set the file editor used e.g. by git-commit:
export EDITOR=vim
export VISUAL="$EDITOR"

# do not create “core” files when a program crashes:
ulimit -c 0

# less: do not create history file:
export LESSHISTFILE=-

# OCaml: print stack traces (for program compiled with flag ‘-g’):
export OCAMLRUNPARAM=b

# start an SSH agent or load an existing one:
function load-ssh-agent() {
	command -V ssh-agent >/dev/null || return 1
	[ -S "$SSH_AUTH_SOCK" ] && return
	declare agent_file="$HOME"/.ssh-agent
	# NOTE: We try to use a lock file, as this function is otherwise not safe
	# with respect to other shells running it concurrently.
	{
		if command -v flock >/dev/null ; then
			LANG=C flock --timeout 0.5 9 || return $?
		fi
		unset SSH_AUTH_SOCK SSH_AGENT_PID
		source "$agent_file" >/dev/null
		if [ ! -S "$SSH_AUTH_SOCK" ] ; then
			ssh-agent 9>&- >"$agent_file" || return $?
			source "$agent_file" >/dev/null
			echo "Started ssh-agent with pid $SSH_AGENT_PID"
		fi
	} 9>>"$agent_file"
}
load-ssh-agent

# Bash: source user Bash config
if test -n "$BASH_VERSION" -a -r "$HOME/.bashrc" ; then
	. "$HOME/.bashrc"
fi
