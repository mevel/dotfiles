# Detach 'screen' instead of killing it when the containing terminal is closed.
autodetach on

# Disable the greeting message.
startup_message off

# Disable the visual bell.
vbell off

# Enable the 256 colors.
term rxvt-unicode-256color
termcapinfo xterm*|rxvt* 'Co#256:AB=\E[48;5;%dm:AF=\E[38;5;%dm'
defbce on
# Also use a brighter color for bold text (as most terminals do).
attrcolor b ".I"
# Render “standout” text as italic (as most terminals do), instead of reversed.
# Note that this also affects 'screen'’s command line and messages.
termcapinfo xterm*|rxvt* 'so=\E[3m:se=\E[m:ZH=\E[3m:ZR=\E[m'
# Enable true colors (only do this if the containing terminal supports it and if
# the version of 'screen' provides this option).
#truecolor on

# Enable the alternate screen.
altscreen on

# Set the size of the scrollback buffer of each window (default is 100 lines).
defscrollback 10000

# Allow to scroll the containing terminal.
#
# By default, 'screen' intercepts the mouse wheel and transmits it to the active
# window. This works well for curses-like programs such as less, man, top or vim
# but, in a shell window, scrolling is interpreted as pressing an arrow key
# several times, ie. it scrolls through the command history.
#
# With the setting below, 'screen' won’t intercept the mouse wheel, so that it
# will scroll through the scrollback buffer of the containing terminal ('screen'
# is displayed in the alternate screen of the terminal to which it is attached).
# This is messy, this is not something we would want, and it breaks scrolling in
# curses-like programs such as less, man or top (vim still manages to interpret
# the scroll whell somehow). However it may prove helpful when running a shell
# session inside of a single 'screen' window.
#
# Still, the only way to scroll through the scrollback buffer of a 'screen'
# window remains to enter copy-mode (C-a Esc).
#
# See also https://stackoverflow.com/a/3474732/4615179
#termcapinfo xterm*|rxvt* ti@:te@

# Allow to switch between regions of a split screen by clicking into them.
defmousetrack on

# On a BÉPO keyboard, '#' is the key left to '1'.
bind \# select 0

# Show a caption line with the hostname and the list of windows (their numbers
# and their names). The current window is highlighted. If the list is longer
# than the available space, it is truncated and centered on the current window.
# This also provides an indicator which tells when the 'screen' shortcut C-a has
# been pressed, and one for the copy-mode.
#
# Note that the padding mechanism is hackish. After much trials, this happens to
# do what we want, but don’t expect it to be easily customizable.
#
#     A = indicator for C-a (' ' or '^')
#     H = hostname, with a bg. color dependent on the indicator (green or red)
#     P = indicator for the copy-mode (' ' or 'P')
#     1 = windows before the current one
#     2 = current window, with highlighting (bold and green foreground)
#     3 = windows after the current one
#     %>, %< and %= are padding instructions.
#                   <--A----------------------------><--H-> <--P----------------->     <1>       <--2------------><3>
caption always '%{=}%?%E%{+b .R}^^%{-}%{Rk}%: %{Gk}%?%H%{-} %?%P%{+b .Y}P%{-}%: %? %0L=%-w%L>%50<%{+b .G}%n %t%{-}%+w'
# Show the caption line at the top (not all versions provide this option).
#caption top

# Allow applications to change the name of the window (to do so, print the
# desired name enclosed between the <esc>k and <esc>\ escape sequences).
defdynamictitle on

# Set the “hstatus” text of each window to a meaningful window title.
#
# Note that this “hstatus” text includes (using the format string %t or ^Et)
# what 'screen' calls the name of the window, which is the text that can be set
# with C-a A and changed dynamically by applications (see above).
#
# ^EH is the hostname, ^En the window’s number, ^Et the window’s name.
defhstatus "[^EH screen $USER] ^En ^Et"
# If the containing terminal provides a “hardware status line”, then use it to
# display the “hstatus” text; otherwise do not display it in any way.
hardstatus ignore '%h'
# Make the titlebar of a containing GUI terminal be the “hardware status line”.
termcapinfo xterm*|rxvt*|kterm*|terminator*|Eterm* 'hs:ts=\E]0;:fs=\007:ds=\E]0;\007'

# Show messages on the screen (in reverse video mode), even when the containing
# terminal provides a “hardware status line”.
hardstatus off
# Show messages at the top left corner (not all versions provide this option).
#status top left

# TODO
# set these terminals up to be 'optimal' instead of vt100 (option -O of 'screen')
termcapinfo xterm*|rxvt*|Eterm*|linux* OP

# Choose our favorite shell (normally this is done via $SHELL, but when 'screen'
# is launched at startup, eg. with cron, this variable is not set).
shell "/bin/bash"
