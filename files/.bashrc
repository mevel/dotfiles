#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# OPAM:
test -r "$HOME/.opam/opam-init/init.sh" && . "$HOME/.opam/opam-init/init.sh" > /dev/null 2> /dev/null || true
# Note: This init script was already sourced by the parent login shell, from
# ~/.profile, but is has to be sourced again from here to benefit from
# interactive features (auto‐completion and the function ‘opam-switch-eval’).

command -v opam >/dev/null 2>&1 && eval $(opam config env)

# remove duplicate opam paths (which exist in PATH because of redundant init)
# and paths to previous opam switches (which may exist in PATH if we have
# switched the opam context since login)
# (this function is defined in ~/.profile):
cleanup_paths PATH
cleanup_paths MANPATH
# for MANPATH, an empty item means the default search locations, rather than
# the current directory (see `man 5 manpath`); in order to find standard pages
# when MANPATH is set, we need to have an empty item:
[ -n "$MANPATH" ] && export MANPATH=":$MANPATH"

#
# Prompt
#

# from the default config (unknown effect):
#PS1='[\u@\h \W]\$ '
#case ${TERM} in
#  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
#    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
#
#    ;;
#  screen*)
#    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
#    ;;
#esac

# colours
case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*|screen*)
	promptfg='\[\e[0;38;5;243m\]'
	cmdfg='\[\e[0;38;5;10m\]'
	;;
  *)
	promptfg='\[\e[1;30m\]'
	cmdfg='\[\e[1;32m\]'
	;;
esac

# show user & host (in prompt and terminal title)?
showhost='yes'

# command prompt (we use a subshell with ‘echo’ to capture the last return
# status and change the decoration colour $c accordingly):
PS1='$(
	if [[ $? == 0 ]]; then
		c=6
	else
		c=1
	fi
	c="\[\e[1;3${c}m\]"
	echo "'$promptfg
# opening parenthesis:
PS1+='$c('$promptfg
# user@host:
[ "$showhost" ] && PS1+=' \u@\h $c:'$promptfg
# working directory:
PS1+=' \w'
# git branch (also works when in detached HEAD state):
#PS1+='$([ -d .git ] || git rev-parse --is-inside-work-dir &>/dev/null && echo -n " $c%'$promptfg' " && git name-rev --name-only HEAD)'
PS1+='$(branchname=$(git name-rev --name-only HEAD 2>&-) && echo -n " $c%'$promptfg' $branchname")'
# closing parenthesis:
PS1+=' $c)'$promptfg
# new line:
PS1+='\n'
# command prompt:
PS1+='$c\$ '$cmdfg
# close the outer ‘echo’ command and the subshell:
PS1+='"
)'

# This allow us to execute something just after a command has been entered
# (before it is executed); thus we can display the command with some colour
# ($cmdfg), then reset default formatting for the output:
#trap 'echo -ne "\e[0m"' DEBUG
# Since Bash 4.4, the variable PS0 does this better:
PS0='\[\e[0m\]'
PS2=$promptfg'›'$cmdfg' '
PS4=$promptfg'+'$cmdfg' '

# prompt for the ‘select’ language construct:
PS3='choix : '

# terminal name
tty=$(tty)
tty=${tty#/dev/}

# Set a title if possible.
#title='['"$tty"'] \u: \W'
#case "$TERM" in
#  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
#	PS1='\[\e]0;'"$title"'\a\]'"$PS1"
#	;;
#  screen)
#	#PS1='\[\e_'"$title"'\e\]'"$PS1"
#	PS1='\[\e]0;'"$title"'\a\]'"$PS1"
#	;;
#esac

case "$TERM" in
  rxvt-unicode*)
	term='urxvt'
	;;
  *)
	term="$TERM"
	;;
esac
#title="[$term$([ "$showhost" ] && echo ' \H') $tty$([ "$showhost" ] && echo ' \u') \W] bash"
title="[$term$([ "$showhost" ] && echo " \H \u") \W] bash"
PS1='\[\e]0;'"$title"'\a\]'"$PS1"

unset promptfg cmdfg
unset tty term title

#
# Misc Bash options
#

# (See bash(1) and readline(3) for available options.)

# run the last command of a pipe in the current shell, instead of a new process:
shopt -s lastpipe

# automatically update the variables $LINES and $COLUMNS if a display is found
# (this is not necessary for an interactive shell, because then these variables
# are already updated on receiving the signal SIGWINCH):
#[[ $DISPLAY ]] && shopt -s checkwinsize

# fix spelling errors with directory completion and ‘cd’:
shopt -s dirspell cdspell

# typing a directory name instead of a command calls ‘cd’:
shopt -s autocd

# load the z utility to navigate quickly through frequently visited directories:
#export _Z_NO_RESOLVE_SYMLINKS=true
export _Z_EXCLUDE_DIRS=( "$HOME/.img" )
[ -r /usr/share/z/z.sh ] && . /usr/share/z/z.sh

# highlight an opening parenthesis when a matching closing parenthesis is typed:
bind 'set blink-matching-paren on'

# automatically insert closing parentheses, quotes and so on:
#     https://superuser.com/questions/629941/quote-or-bracket-completion-in-bash-without-pressing-tab
readline-brackets() {
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}${1}${READLINE_LINE:$READLINE_POINT}"
    ((READLINE_POINT+=1))
}
#bind -x '"\"" : "readline-brackets \"\""'
#bind -x $'"\047" : "readline-brackets \\\047\\\047"'
##bind -x '"<" : "readline-brackets \<\>"'
#bind -x '"(" : "readline-brackets \(\)"'
#bind -x '"[" : "readline-brackets \[\]"'
#bind -x '"{" : "readline-brackets {}"'

# pasting into the command line is treated as only one insertion, instead of one
# per character; this prevents characters (such as newlines) to be interpreted:
bind 'set enable-bracketed-paste on'

# disable XOFF (pause transmission, Ctrl-S) / XON (resume transmission, Ctrl-Q),
# so that Ctrl-S will not freeze the terminal:
stty -ixon
# (Ctrl-S is now bound to forward history search, dual of Ctrl-R.)

# options related to the history:
HISTCONTROL=ignoredups:ignorespace
shopt -s cmdhist lithist histverify histreedit
HISTSIZE=-1

#
# Options related to globbing
#

# a pattern which matches no filename expands to nothing, rather than to itself
# (Note: zero was introduced by Brahmagupta in 628, therefore this option may
# break compatibility with older shells):
shopt -s nullglob
# a pattern which matches no filename causes an error:
shopt -s failglob
# star patterns also apply to filenames beginning with a dot (except . and ..):
shopt -s dotglob
# enable the ** pattern, which matches any file under any path:
shopt -s globstar
# enable extended patterns:
#     @(pat1|pat2|…) matches a disjunction of patterns;
#     !(pat1|pat2|…) matches everything but the excluded patterns;
#     *(pat1|pat2|…), +(pat1|pat2|…), ?(pat1|pat2|…) are multipliers.
shopt -s extglob

#
# Options related to completion
#

# completion is disabled when the command line is empty:
shopt -s no_empty_cmd_completion

# asking for completion on e.g. “$test/<Tab>” will expand the variable ‘$test’
# instead of escaping the dollar sign:
#shopt -s direxpand
# 2024-06: as of Bash 5.2.26 this option seems unneeded
# (it seems that Bash is now able to complete on stuff like “$HOME/” or
# “../$ME/” without expanding the variable in the user’s input line)
# and worse, it causes harm (it unexpectedly expands tilde and relative paths
# when the path to be completed contains spaces, which looks like a bug in Bash;
# possibly fixed by this commit, due in Bash 5.3 ?
# https://git.savannah.gnu.org/cgit/bash.git/commit/?h=devel&id=136cdf81 ).

# do not suggest "." nor ".." nor "stuff/." nor "stuff/.." (nor, technically, 
# anything that ends with "."), unless it is the only match.
# Note that as of Bash 5.2.26, this ignores "..", "stuff/." and "stuff/..", but 
# it fails to ignore "." itself, because FIGNORE applies to proper suffixes. 
# This seems(?) to have been changed in the development version of Bash in 2023 
# (https://lists.gnu.org/archive/html/bug-bash/2023-04/msg00045.html), so it may 
# land at some point.
export FIGNORE='.'
shopt -u force_fignore

# do not suggest hidden files unless a leading dot has explicitly been typed:
bind 'set match-hidden-files off'

# completion ignores case:
bind 'set completion-ignore-case on'
# completion ignores distinction between hyphen and underscore:
bind 'set completion-map-case on'

# a completed name which is a directory, or a symbolic link to a directory, gets
# a slash appended:
bind 'set mark-directories on'
bind 'set mark-symlinked-directories on'

# when completing in the middle of a word, do not duplicate the completed text:
bind 'set skip-completed-text on'

# when there are several completion candidates and they don’t share a common
# prefix, show them immediately:
bind 'set show-all-if-unmodified on'
# when there are several completion candidates, show them immediately:
#bind 'set show-all-if-ambiguous on'

# show completion candidates with the common prefix highlighted:
bind 'set colored-completion-prefix on'
# show completion candidates with the colors given by $LS_COLORS:
bind 'set colored-stats on'
# show completion candidates with the file type indicators (slash, at sign…):
bind 'set visible-stats on'

# Ctrl+J / Ctrl+K cycle through completion candidates:
bind '"\C-j": menu-complete'
bind '"\C-k": menu-complete-backward'

# load the bash-completion package:
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# workaround for bash-completion not supporting some Bash options, see
#     https://github.com/scop/bash-completion/issues/324
#
# As of bash-completion 2.14 this is now fixed, the workaround is unneeded.
#
#if [ $? -eq 0 ] ; then
#	# We disable the unsupported options when typing a command line, and
#	# re-enable them before Bash expands and executes the command line.
#	function save_shell_options() {
#		# NOTE: Because of a bug with 'eval' and 'set -o history', we must
#		# exclude the history from this dump.
#		_saved_set="$(set +o | grep -v ' history$')"
#		_saved_shopt="$(shopt -p nullglob failglob)"
#		set +o nounset
#		shopt -u nullglob failglob
#	}
#	function restore_shell_options() {
#		[ -z "${_saved_set+x}" ] && return
#		set +o history
#		eval "$_saved_set"
#		eval "$_saved_shopt"
#		unset _saved_set _saved_shopt
#		set -o history
#	}
#	# We use the PROMPT_COMMAND variable to run a command just before showing
#	# the prompt. NOTE: For some reason, changes to shell options only take
#	# effect if this is the *last* command executed by PROMPT_COMMAND.
#	PROMPT_COMMAND="$PROMPT_COMMAND
#	save_shell_options;"
#	# We use the DEBUG trap to run a command after the command line is entered,
#	# but before it is expanded by Bash. NOTE: This is very hackish. The manual
#	# is not explicit as whether the trap is actually called *before* expansion,
#	# even though that’s what happens in practice. Moreover, the trap is called
#	# before each *simple* command, including before commands in PROMPT_COMMAND.
#	trap="$(trap -p DEBUG)" ; trap="${trap#trap -- }" ; trap="${trap% DEBUG}"
#	eval "trap 'restore_shell_options;'$trap DEBUG"
#	unset trap
#	# We cannot use PS1 and PS0, as we would only have access to a subshell.
#fi

#
# Aliases
#

# aliases and custom commands were moved to a separate file, so that they can be
# sourced even from a non‐interactive shell (whereas the current file,
# ~/bash.bashrc, specifically targets interactive shells). this is useful for
# example to have aliases work from Vim.
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

#
# Other
#

# Stupid workaround to a silly bug from urxvt:
#	http://superuser.com/questions/442589/xmonad-urxvt-issue-text-disappears-after-resizing
#if [[ $TERM == rxvt-unicode* ]]; then
#	for (( i=0; i<=75; i++ )); do echo; done; clear
#fi
