#
# ~/.bash_aliases
#

# Avoid sourcing this file twice, as aliases and functions would accumulate.
# NOTE: We might instead start by clearing all existing aliases (`unalias -a`)
# but this would also clear aliases set up from other pieces of configuration,
# such as the z utility.
[ -n "$HAS_SOURCED_BASH_ALIASES" ] && return
export HAS_SOURCED_BASH_ALIASES=true

# Always expand aliases, even in non-interactive shells (useful for example to
# be used from Vim).
shopt -s expand_aliases

# test whether a command is available.
#   USAGE:  has_command CMDNAME
function has_command() {
	command -v "$1" >/dev/null 2>&1
}

# This function allows to define aliases incrementally, that is, to add
# arguments to an existing alias (in other words, ALIAS_NAME can be equal to
# ALIASED_NAME).
#   USAGE:  incalias ALIAS_NAME ALIASED_NAME ARG1 ARG2 …
function incalias() {
	local alias_name="$1"
	local aliased_name="$2"
	local quoted_args=''
	if [[ $# -gt 2 ]] ; then
		printf -v quoted_args ' %q' "${@:3}"
	fi
	local type ; type="$(type -t "$aliased_name")"
	if [[ $? -eq 0 && "$type" == 'alias' && "$alias_name" == "$aliased_name" ]]; then
		local expanded="$(alias "$aliased_name")"
		eval "expanded=${expanded#*=}"
		alias "$alias_name=$expanded$quoted_args"
	else
		alias "$alias_name=$aliased_name$quoted_args"
	fi
}

# define shorter names for most common commands
incalias l ls
incalias ll ls -l
incalias la l -A
incalias lla ll -A
incalias n nnn
incalias e echo
incalias p printf
incalias c cat
incalias le less
incalias gr grep # but we use `rg` (ripgrep), see below
incalias d diff
if has_command dwdiff && dwdiff --version >&- 2>&- ; then
	# `dwdiff` is a better alternative to `wdiff`,
	# but it comes from the AUR and sometimes breaks when libraries are updated:
	incalias wd dwdiff
elif has_command wdiff ; then
	incalias wd wdiff
fi
incalias v vim
incalias g git
incalias md mkdir
incalias ren rename
incalias pren perl-rename
incalias chm chmod
incalias cho chown
incalias m man
incalias about apropos
incalias sd systemctl
incalias s sudo

# colors!
test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)"
incalias ls ls --color=auto
incalias dir dir --color=auto
incalias vdir vdir --color=auto
incalias grep grep --color=auto
incalias fgrep fgrep --color=auto
incalias egrep egrep --color=auto
incalias diff diff --color=auto
incalias dwdiff dwdiff --color
if has_command wdiff && has_command tput ; then
	function wdiff() {
		if test -t 1 ; then
			command wdiff \
			  -w "$(tput setaf 1)[-$(tput bold)" \
			  -x "$(tput sgr0; tput setaf 1)-]$(tput sgr0)" \
			  -y "$(tput setaf 2){+$(tput bold)" \
			  -z "$(tput sgr0; tput setaf 2)+}$(tput sgr0)" \
			  "$@"
		else
			command wdiff "$@"
		fi
	}
fi
incalias ip ip -c
incalias gcc gcc -fdiagnostics-color=auto # gcc >= 4.9
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
incalias less less -R

# make standard commands more human-friendly (verbose, interactive, safer)
incalias ls ls -vhF --hyperlink=auto --time-style=+" %x  
 %d/%m %H:%M "
incalias less less -MiS # see also `bat` below
incalias diff diff -ru
incalias mkdir mkdir -pv
incalias ln ln -fnv
incalias mv mv -iv
incalias rename rename -iv
incalias perl-rename perl-rename -iv
incalias cp cp -Riv --preserve=all
incalias scp scp -rp
incalias rsync rsync -aAXE -hv --progress --partial --append-verify
incalias dd dd status=progress
incalias rm rm  -iv --one-file-system --preserve-root=all
incalias chmod chmod -c --preserve-root
incalias chown chown -c --preserve-root -h
incalias du du -h
incalias df df -hT

# recursive deletion of a directory which asks for confirmation once
incalias rd rm -RIv --one-file-system --preserve-root=all

# switching to another directory and listing its contents
function cl() { dir="$1" ; shift ; cd -- "$dir" ; ls "$@" ; }

# bat is a replacement for cat with syntax highlighting, line numbering, git
# diffing and paging
if [ -x /usr/bin/bat ] ; then
	# options that `less` requires for paginating `bat` output:
	#   -R: support colors and formatting (keep ANSI escape codes)
	#   -L: avoid double-processing the output (ignore LESSOPEN)
	# options that make `less` more user-friendly (see also `less` alias above):
	#   -M: more verbose prompt
	#   -i: case-insensitive search
	#   -S: do not wrap long lines
	# more `less` options:
	#   -F: do not paginate if the output is less than one screen
	#
	# (1) `bat`, `b`: syntax coloring, decoration, paginate if too long
	# NOTE: when `bat` is piped, it falls back to a row `cat` output
	export BAT_PAGER="less -RL -MiS -F"
	# show file names, line numbers, git changes
	# but hide the grid (useless and space-consuming):
	incalias bat bat --style=header,numbers,changes,snip --italic-text=always
	incalias b bat
	# (2) `cat`, `c`: syntax coloring, no decoration, never paginate
	#  => replacement for `cat`
	incalias cat bat --paging=never --style=plain --tabs=0 --wrap=never
	# (3) `bless`, `bl`: syntax coloring, decoration, always paginate
	#  => replacement for `less`
	#     (not exactly equivalent, since `bat` concatenates all input files):
	incalias bless bat --paging=always --pager='less -RL -MiS' --tabs=0 --wrap=never
	incalias bl bless
	# colorize `less`:
	# 2021-08-31: deactivated as too intrusive: lots of programs use `less`
	# internally, e.g. `nnn` help page, or `git log`, and we don’t want to mess
	# them with useless line numbers nor garbage escape codes.
	#export LESSOPEN="||- bat --paging=never --wrap=never --color=always --decorations=always --style=numbers,changes,snip %s"
	# colorize man pages (the MANROFFOPT tweak is required since groff 1.23,
	# see bat’s README and https://github.com/sharkdp/bat/issues/2668 ):
	export MANPAGER="sh -c 'col -bx | bat --pager=\"less -RL -MiS\" -l man -p'"
	export MANROFFOPT="-c"
fi

# rg (“ripgrep”) is a modern replacement for grep
#   * can filter by filename patterns, with predefined patterns (“types”)
#      -> restrict: -g '*.ml'         -t cpp
#      -> exclude:  -g '!*.cm[io]'    -T cpp
#      -> some predefined types: sh, cpp, coq, ocaml
#      -> to register types: --type-add in a shell alias or in the rg conf file
#   * exclude .gitignored files by default      -> include them: --no-ignore
#   * exclude hidden files by default           -> include them: --hidden (-.)
#   * exclude binary files by default           -> include them: --text (-a)
#   * can search into compressed files (no tar) -> enable: -z
#   * auto-detect and translate UTF-16-encoded files (regexes are UTF-8)
#   * support custom preprocessors
#   * recursive by default, parallelized, very fast
#
# NOTE: For git repos, “git grep” is even better: it searches into the tracked
# files (so it ignores untracked files and, conversely, it searches into files
# that match .gitignore rules but that are tracked nonetheless).
if [ -x /usr/bin/rg ] ; then
	# exclude .gitignored files even when not in a git repo:
	incalias rg rg --follow --no-require-git
	# use smarter patterns:
	incalias rg rg --engine auto --multiline-dotall --smart-case
	# print line numbers even when output is piped:
	#incalias rg rg -n
fi

# only search patterns in base name (otherwise all descendents of a matching
# directory are reported, which is pointless)
incalias locate locate -b

# make vim use one tab per file given on the command line
incalias vim vim -p
incalias vi vim

# make bc quiet
incalias bc bc -q
# bc loads the math library and a resource file, if available
function bc() {
	local args=( -l )
	[[ -r ~/.bcrc ]] && args+=( ~/.bcrc )
	command bc "${args[@]}" "$@"
}
# an inline version of bc
#   USAGE:  = [BC OPTIONS] [--] MATH EXPR
# the math expression cannot start with a minus sign, unless -- has been given.
function =() {
	local args=( )
	for a ; do
		if [[ "$a" == '--' ]] ; then
			shift
			break
		elif [[ ! "$a" =~ ^- ]] ; then
			break
		fi
		args+=( "$a" )
		shift
	done
	bc "${args[@]}" <<< "$*"
}

# display the number of arguments it has been called with.
# a hack is used so that a command like ”count globpat” works as expected when
# there is zero match for a given globbing pattern, whatever the shell options.
# from https://stackoverflow.com/questions/11307257/is-there-a-bash-command-which-counts-files/57436073#57436073
#   USAGE:  count PATTERN1 PATTERN2 …
#   EXAMPLES:  count *                  # count all files in current directory
#              count GLOBPAT1 GLOBPAT2  # count files matching one of patterns
#              count $(jobs -ps)        # count stopped jobs
function count() {
	eval "$_count_saved_shopts"
	unset _count_saved_shopts
	echo $#
}
# it all works because aliases are expanded before globbing patterns.
alias count='
	_count_saved_shopts="$(shopt -p nullglob failglob)"
	shopt -s nullglob
	shopt -u failglob
	count'

# display the cumulated size of the arguments.
#   USAGE:  totaldu -sc [ARGS]
function totaldu() {
	du -sc "$@" | tail -1 | cut -f1
}
# `du` sorted by path
function dua() { du "$@" | sort -k2 ; }
# `du` sorted by size (decreasing)
function dus() { du "$@" | sort -hr ; }

# change owner and permissions recursively for files of the type specified
# (d for directories, f for regular files, l for links and so on).
#   USAGE:  rchmodown TYPE MODE OWNER [FIND_TARGETS_AND_OPTIONS]
#           rchmod TYPE MODE [FIND_TARGETS_AND_OPTIONS]
function rchmodown() {
	t="$1"
	m="$2"
	o="$3"
	shift 3
	find "$@" -type "$t" \
	  -printf "%M %8u:%-8g %p\n" \
	  -and -exec chown "$o" {} \+ \
	  -and -exec chmod "$m" {} \+
}
function rchmod() {
	t="$1"
	m="$2"
	shift 2
	find "$@" -type "$t" \
	  -printf "%M %8u:%-8g %p\n" \
	  -and -exec chmod "$m" {} \+
}

# ‘findmnt’ is much better than plain ‘mount’ for visualizing mounted devices
incalias lsmount findmnt --real -o TARGET,SOURCE,LABEL,SIZE,AVAIL,FSTYPE,OPTIONS

# handy shortcuts to mount encrypted devices.
#   USAGE:  luks-mount DEVICE [MAPPED_NAME [MOUNT_OPTIONS]]
#           luks-umount [MAPPED_NAME]
function luks-mount() {
	device="$1"
	[ -z "$device" ] && return 1
	mapped_name="$2"
	[ -z "$mapped_name" ] && mapped_name="secrets"
	shift 2
	sudo cryptsetup open "$device" "$mapped_name" \
	&& sudo mount /dev/mapper/"$mapped_name" "$@"
}
function luks-umount() {
	mapped_name="$1"
	[ -z "$mapped_name" ] && mapped_name="secrets"
	sudo umount /dev/mapper/"$mapped_name" \
	&& sudo cryptsetup close "$mapped_name"
}

# use the existing ‘kill’ command instead of the Bash builtin (more powerful)
enable -n kill
# terminate all processes (jobs) running under the current shell
alias killjobs='kill -9 $(jobs -p)'

# custom output format for ‘ps’:
export PS_FORMAT=pid,user,pmem,s,cmd

# grep through processes.
function pg() {
	ps=( \ps --forest --width="$COLUMNS" -o pid,user,pmem,s,cmd )
	# print just the headers:
	"${ps[@]}" -NA
	# filter all processes with ‘grep’:
	"${ps[@]}" --no-headers -A | \grep --color=auto -C3 -Pi \
		"^(?!.*EXCLUDE-MYSELF-SELF-REFERENTIALLY-AH-AH-AH-$RANDOM).*\K$@"
}

# launch and detach an application without cluttering the console
function launch {
	nohup &>/dev/null "$@" &
	#disown # this always refer to the latest job started in background (Bash 5.1)
	# FIXME 2021-08-17: wrong! my Bash keeps killing other paused jobs!
}
incalias vlc launch vlc
incalias gimp launch gimp

# show accurate memory usage of processes (computes the PSS metric, where each
# process has a fair fraction of the pages it shares with other processes)
incalias smem smem -katp
incalias pmem smem -rs pss -c 'pid user pss swap name command'
# show a per-user summary
incalias umem smem -urs pss -c 'user count pss swap'
# show a system-wide summary
incalias wmem smem -w

# show info about the package owning given files
#   USAGE:  frompkg FILE1 FILE2 …
function frompkg() {
	#pacman -Qi $(pacman -Qqo -- "$@")
	[ $# -eq 0 ] && return
	declare -a pkg ; pkg=( $(pacman -Qqo -- "$@") )
	declare -i ret=$?
	[ ${#pkg[@]} -ne 0 ] && pacman -Qi "${pkg[@]}"
	[ $? -eq 0 -a $ret -eq 0 ]
}
# show info about the package owning given programs
#   USAGE:  prgmfrompkg PRGM1 PRGM2 …
function prgmfrompkg() {
	#frompkg "$(which -- "$@")"
	[ $# -eq 0 ] && return
	declare -a prgm ; prgm=( $(which -- "$@") )
	declare -i ret=$?
	frompkg "${prgm[@]}"
	[ $? -eq 0 -a $ret -eq 0 ]
}

# the French localization of GnuPG is crappy
alias gpg='LC_ALL=C gpg'
# GnuPG is deliberately broken for downloading/refreshing keys from the new key
# servers such as keys.openpgp.org which appeared after the 2019 DoS attack.
# This is because these servers often send keys with no user ID. See also
# https://keys.openpgp.org/about/usage and https://dev.gnupg.org/T4393 .
# Here is a working command to refresh keys from the Ubuntu server:
alias gpg-refresh-keys='gpg --list-keys | grep ^pub -A 1 | grep -v - | sort -u | xargs -t -I {} curl "http://keyserver.ubuntu.com/pks/lookup?op=get&search=0x{}"'

# recursive downloading of a remote directory (add --cut-dirs=N to drop N levels
# of the directory structure, and do not forget to append a final ‘/’ to the URL
# in order not to download the directory’s siblings)
incalias rwget wget -r -nH --no-parent --reject 'index.html\*' -m -e robots=off

# do not print commands fired by make
#incalias make make -s
# disable make’s built-in implicit rules and suffixes
incalias make make -r

# do not use localized output for gcc (ugly, messy, not google-friendly)
alias gcc="LC_ALL=C $(alias gcc |cut -d\' -f2)"
# just for fun: gcc with a personal set of options
GCC_SOURCE_CHARSET=UTF-8
GCC_EXEC_CHARSET=UTF-8
GCC_WIDE_EXEC_CHARSET=UTF-32
incalias gcc gcc \
  -fmax-errors=5 -ftabstop=4 \
  -O2 -Wdisabled-optimization \
  -DSOURCE_CHARSET=\"$GCC_SOURCE_CHARSET\"       -finput-charset=$GCC_SOURCE_CHARSET \
  -DEXEC_CHARSET=\"$GCC_EXEC_CHARSET\"           -fexec-charset=$GCC_EXEC_CHARSET \
  -DWIDE_EXEC_CHARSET=\"$GCC_WIDE_EXEC_CHARSET\" -fwide-exec-charset=$GCC_WIDE_EXEC_CHARSET \
  -std=c11 \
  -fextended-identifiers \
  -include stdbool.h -include stdalign.h -include stdnoreturn.h \
  -Wall -Wextra -Wpedantic -Wuninitialized -Wjump-misses-init -Wswitch-default -Wbad-function-cast -Wcast-qual -Wcast-align -Wconversion -Wwrite-strings -Wformat=2 -Wfloat-equal -Wlogical-op -Waggregate-return -Wstrict-prototypes -Wold-style-definition -Wunused-macros -Wpadded -Wredundant-decls -Wnested-externs -Winline -Wvla \
  -Wmissing-include-dirs -Winvalid-pch -Wunknown-pragmas

# launch coqide without cluttering the console
incalias coqide launch coqide

# handle arrow keys and all that stuff in the OCaml interpreter
incalias ocaml rlwrap ocaml

# sync the environment variables with respect to OPAM’s switch
function opam-eval {
	eval $(opam config env)
	# MANPATH has been deprecated on ArchLinux but OPAM keeps setting it, which
	# prevents bash from searching for adequate autocompletion outside of OPAM’s
	# directory; man pages are almost absent of OPAM anyway…
	#unset MANPATH
	# 2018-08-26: In fact, setting MANPATH does not prevent ‘man’ / ‘apropos’
	# from finding man pages (or at least, not anymore). Indeed, ‘man’ always
	# inspect PATH in addition to MANPATH.
	#
	#     $  MANPATH= man -kd Scanf
	#     ...
	#     path directory /home/me/.opam/4.06.1/bin is not in the config file
	#     but does have a ../man, man, ../share/man, or share/man subdirectory
	#     adding /home/me/.opam/4.06.1/man to manpath
	#     ...
	#
	# What still does not work on my computer is Bash autocompletion, but this
	# seems an unrelated issue (whether MANPATH is set or not does not fix it).
	# 2019-07-25: The autocompletion bug was unrelated to MANPATH, it was a bug
	# in bash-completion which has since been fixed.
}
# select OPAM’s switch and set the adequate environment variables
function opam-switch {
	opam switch "$@"
	opam-eval
}
# Note: OPAM’s init script defines ‘opam-switch-eval’ with the same
# functionality, except that it sets MANPATH. This one is preferred.
# This shouldn’t be useful any more since opam 2.0 adds a hook to the prompt.
incalias opam-switch-eval opam-switch

# copy to and paste from the graphical clipboard (the one used by ^C/^V)
incalias xcc xclip -i -sel c
incalias xcp xclip -o -sel c
# the same for “rich” contents (ie. HTML), for interaction with eg. a web
# browser or word processor
incalias xcchtml xclip -i -sel c -t text/html
incalias xcphtml xclip -o -sel c -t text/html

# open files using the associated applications.
#   USAGE:  x FILE1 FILE2 …
function x() {
	# NOTE: “gio open” does exactly what we want: it accepts several files, it
	# is asynchronous, and it does not leave a parent process once applications
	# are launched; however it is specific to some desktop environments.
	if has_command gio ; then
		gio open "$@"
	else
		( for file ; do exec xdg-open "$file" & done )
	fi &>/dev/null
	return 0
}

# display a GIF animation
incalias gifview gifview -a --fallback-delay 5
incalias gif gifview

# allow feh to open more types of files, such as SVGZ, OTF, TTF, PS, EPS, PDF…
# by using libconvert (from ImageMagick).
incalias feh feh --conversion-timeout 3
# options to feh that makes it more suitable for a tiling window manager
incalias feh feh --no-screen-clip --scale-down
# other options
incalias feh feh --title '(%u/%l) %f [%z]' --sort name --version-sort --alpha 0

# sanitize output from ffmpeg
incalias ffmpeg ffmpeg -hide_banner -loglevel level+info
incalias ffprobe ffprobe -hide_banner -loglevel level+info

# a calculator supporting storage size units (such as "4MiB + 1.7g")
incalias bcal bscalc

# encode/decode Quoted-Printable.
#   USAGE:  qpe 'TEXT TO ENCODE' …
#           qpd 'TEXT TO DECODE' …
#       or  qpe < FILE
#           qpd < FILE
alias _qpe="perl -MMIME::QuotedPrint -pe '\$_=MIME::QuotedPrint::encode(\$_);'"
alias _qpd="perl -MMIME::QuotedPrint -pe '\$_=MIME::QuotedPrint::decode(\$_);'"
function qpe() {
	if [ $# -eq 0 ] ; then
		_qpe
	else
		echo "${@}" | qpe
	fi
}
function qpd() {
	if [ $# -eq 0 ] ; then
		_qpd
	else
		echo "${@}" | qpd
	fi
}

# Convert an image (of any type) to WEBP, losslessly.
#   USAGE:  png2webp PNGFILE OPTIONS…
function 2webp() {
    srcname="$1" ; shift
    convert "$srcname" -define webp:lossless=true "$@" "${srcname%.png}.webp"
}

# Show weather at the current (auto-detected) place, or at a designated one.
#   USAGE:  meteo [NAME OF PLACE]
function meteo() {
	curl fr.wttr.in/`tr -s ' ' '_' <<< "$*"`
}

# execute xev (print keys entered), but output a much more concise output:
alias printkeys="xev | grep -A2 --line-buffered '^KeyRelease' | sed -n '/keycode/s/^.*keycode \([0-9]*\).* (.*, \(.*\)).*$/\1 \2/p'"

#
# Completion for the above aliases
#

# FIXME: According to the Bash manual, this should set programmable completion
# for aliases, mimicking that of the aliased commands. Unfortunately, this does
# not seem to work. Worse, it apparently mangles the input line with an error
# message. Hence, we have to resort to all that tricky stuff below.
#shopt -s progcomp_alias

# If bash-completion is not enabled, do not do anything:
type -t _completion_loader >/dev/null || return

# This function produces a string which represents its sequence of arguments in
# Bash syntax (in other words, in a format suitable for ‘eval’). For example,
# the following works as expected:
#     eval "cmd $(quote "$@")"
# Arguments are separated by spaces and each argument is properly quoted.
function quote {
	if [ $# -ne 0 ] ; then
		printf '%q' "$1"
		if [ $# -ne 1 ] ; then
			printf ' %q' "${@:2}"
		fi
	fi
}

# This function declares a function which can be used for completion.
# Example use:
#     alias gst='git status'
#     make-completion-wrapper _gst_completion _git_completion git status
#     complete -F _gst_completion gst
# The function _gst_completion wraps the function _git_completion, making sure
# that the wrapped function believes that the command line starts with the two
# words “git”, “status”, while in reality it starts with an arbitrary word which
# is ignored. Hence, when typing the alias “gst”, completion behaves as if “git”
# “status” was typed.
# Adapted from https://ubuntuforums.org/showthread.php?t=733397
function really-make-completion-wrapper () {
	local wrapper_function_name="$1"
	local wrapped_function_name="$2"
	shift 2
	local words=( "$@" )
	local word_count=$#
	local concatenated_words="$@"
	local words_length=${#concatenated_words}
	eval '
		function '"$wrapper_function_name"' {
			local args=( "$@" )
			args[0]='"$(quote "${words[0]}")"'
			if [ "$COMP_CWORD" -eq 1 ] ; then
				args[2]='"$(quote "${words[-1]}")"'
			fi
			(( COMP_CWORD += '"$word_count"' - 1 ))
			(( COMP_POINT += '"$words_length"' - ${#COMP_WORDS[0]} ))
			COMP_WORDS=( '"$(quote "${words[@]}")"' "${COMP_WORDS[@]:1}" )
			# This will break the cursor position if the initial command line
			# has more than one space between some words:
			COMP_LINE="${COMP_WORDS[@]}"
			type -t '"$wrapped_function_name"' >/dev/null 2>&1 || \
				_completion_loader '"$(quote "${words[0]}")"'
			'"$wrapped_function_name"' "${args[@]}"
		}'
}
# Improve Bash startup time by building the completion function lazily:
function make-completion-wrapper() {
	eval '
		function '"$1"' {
			really-make-completion-wrapper '"$(quote "$@")"'
			'"$1"' "$@"
		}
	'
}


# To find the completion function for “git”, type “git ”, press tab, then run:
#     complete -p git

make-completion-wrapper _ls_complwrapper _comp_complete_longopt ls
complete -F _ls_complwrapper ll
complete -F _ls_complwrapper l
complete -F _ls_complwrapper lla
complete -F _ls_complwrapper la
complete -F _ls_complwrapper cl

make-completion-wrapper _nnn_complwrapper _nnn nnn
complete -F _nnn_complwrapper n

make-completion-wrapper _cat_complwrapper _comp_complete_longopt cat
complete -F _cat_complwrapper c

make-completion-wrapper _less_complwrapper _comp_complete_longopt less
complete -F _less_complwrapper le

make-completion-wrapper _grep_complwrapper _comp_complete_longopt grep
complete -F _grep_complwrapper egrep
complete -F _grep_complwrapper fgrep
complete -F _grep_complwrapper gr

make-completion-wrapper _diff_complwrapper _comp_complete_longopt diff
complete -F _diff_complwrapper d

complete -F _comp_complete_longopt dwdiff
make-completion-wrapper _dwdiff_complwrapper _comp_complete_longopt dwdiff
complete -F _dwdiff_complwrapper wd

make-completion-wrapper _vim_complwrapper _comp_complete_filedir_xspec vim
complete -F _vim_complwrapper v

make-completion-wrapper _git_complwrapper __git_wrap__git_main git
complete -o bashdefault -o default -o nospace -F _git_complwrapper g

make-completion-wrapper _mkdir_complwrapper _comp_complete_longopt mkdir
complete -F _mkdir_complwrapper md

make-completion-wrapper _rename_complwrapper _rename_module rename
complete -F _rename_complwrapper ren

make-completion-wrapper _rm_complwrapper _comp_complete_longopt rm
complete -F _rm_complwrapper rd

make-completion-wrapper _chmod_complwrapper _comp_cmd_chmod chmod
complete -F _chmod_complwrapper chm

make-completion-wrapper _chown_complwrapper _comp_cmd_chown chown
complete -F _chown_complwrapper cho

make-completion-wrapper _apropos_complwrapper _comp_cmd_man apropos
complete -F _apropos_complwrapper about

make-completion-wrapper _systemctl_complwrapper _systemctl systemctl
complete -F _systemctl_complwrapper sd

make-completion-wrapper _sudo_complwrapper _comp_cmd_sudo sudo
complete -F _sudo_complwrapper s

if [ -x /usr/bin/bat ] ; then
	make-completion-wrapper _bat_complwrapper _bat bat
	complete -F _bat_complwrapper b
	complete -F _bat_complwrapper cat
	complete -F _bat_complwrapper c
	complete -F _bat_complwrapper bless
	complete -F _bat_complwrapper bl
fi

make-completion-wrapper _du_complwrapper _comp_complete_longopt du
complete -F _du_complwrapper totaldu
complete -F _du_complwrapper dua
complete -F _du_complwrapper dus

make-completion-wrapper _find_type_complwrapper _comp_cmd_find find -type
complete -F _find_type_complwrapper rchmodown
complete -F _find_type_complwrapper rchmod

make-completion-wrapper _findmnt_complwrapper _findmnt_module findmnt
complete -F _findmnt_complwrapper lsmount

make-completion-wrapper _mount_complwrapper _mount_module mount
complete -F _mount_complwrapper luks-mount

make-completion-wrapper _kill_complwrapper _comp_cmd_kill kill
complete -F _kill_complwrapper killjobs

complete -c launch

complete -c prgmfrompkg

make-completion-wrapper _wget_complwrapper _comp_cmd_wget wget
complete -F _wget_complwrapper rwget

make-completion-wrapper _opam_switch_complwrapper _opam opam switch
complete -F _opam_switch_complwrapper opam-switch
complete -F _opam_switch_complwrapper opam-switch-eval

complete -F _comp_complete_longopt gifview
make-completion-wrapper _gifview_complwrapper _comp_complete_longopt gifview
complete -F _gifview_complwrapper gif

make-completion-wrapper _convert_complwrapper _comp_cmd_convert convert
complete -F _convert_complwrapper 2webp

#
# Completion for scripts in ~/bin
#

complete -F _comp_complete_longopt tvrec
complete -F _comp_complete_longopt cropvid
complete -F _comp_complete_longopt rescale-subs
complete -F _comp_complete_longopt toutes-lettres
