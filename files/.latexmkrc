# Configuration for latexmk. See https://mg.readthedocs.io/latexmk.html

# Remove more things when cleaning.
$clean_ext = "bbl nav out snm tns vrb synctex.gz";

# The package <svg> requires the option -shell-escape to be able to call an
# external program.
$latex = 'latex -shell-escape';
$pdflatex = 'pdflatex -shell-escape';

# NOTE: vimtex calls latexmk with option -interaction=nonstopmode, which ignores
# all errors and produces a PDF anyway. Remember to check that the document does
# compile without this option as well.
