# custom colorscheme, adapted from xfce4-terminal’s defaults (good contrast)

color_background='#303030'
color_foreground='#ccffcc'

color_black='#454545'
color_black_light='#5f5f5f'

color_red='#aa0000'
color_red_light='#ff5555'

color_green='#00aa00'
color_green_light='#55ff55'

color_yellow='#ffaa00'
color_yellow_light='#ffff55'

color_blue='#2828d2'
color_blue_light='#5555ff'

color_magenta='#aa00aa'
color_magenta_light='#ff55ff'

color_cyan='#4fc0c0'
color_cyan_light='#55ffff'

color_white='#b4b4b4'
color_white_light='#ffffff'
