# Tango colorscheme
# https://en.wikipedia.org/wiki/Tango_Desktop_Project#Palette

# standard colors:
color_background='#000000'
color_foreground='#babdb6'
# softer tones:
color_background='#101010'
color_foreground='#a09090'

color_black='#2e3436'
color_black_light='#555753'

color_red='#cc0000'
color_red_light='#ef2929'

color_green='#4e9a06'
color_green_light='#8ae234'

color_yellow='#c4a000'
color_yellow_light='#fce94f'

color_blue='#3465a4'
color_blue_light='#729fcf'

color_magenta='#75507b'
color_magenta_light='#ad7fa8'

color_cyan='#06989a'
color_cyan_light='#34e2e2'

color_white='#d3d7cf'
color_white_light='#eeeeec'
