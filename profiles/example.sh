username=me
personal_server=myserver.com
personal_server_username=pi
git_user_name='Jean-Baptiste Poquelin'
git_user_mail=jb@poquelin.fr
git_alt_user_name=Molière
git_alt_user_mail=moliere@illustre-theatre.fr
source "$dotfiles_root/profiles/colorscheme_tango.sh"
prior_key=Prior
next_key=Next
lan=eth0
wlan=wlan0
has_battery=true
